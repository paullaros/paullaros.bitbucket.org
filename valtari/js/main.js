$(window).scroll(function () {
    toggleNavbar();
});

function toggleNavbar(){
    if ($(window).scrollTop() > ( $(window).height() ) ) {
        /*
        * Navbar top
        */

        $("body").addClass("navbar-top");

        $("#about").css("margin-top", "51px");

        // Navbar classes
        $(".navbar").addClass("navbar-fixed-top");
        $(".navbar").removeClass("navbar-bottom");
        $(".navbar").removeClass("navbar-fixed-bottom");

        // Toggle navbar style (inverse)
        $(".navbar").addClass("navbar-inverse");
        $(".navbar").removeClass("navbar-default");

        // Animations
        $(".navbar-brand").addClass("fadeInUp");


    } else {
        /*
        * Navbar bottom
        */

        $("body").removeClass("navbar-top");

        $("#about").css("margin-top", "0");

        // Navbar classes
        $(".navbar").removeClass("navbar-fixed-top");
        $(".navbar").addClass("navbar-bottom");
        $(".navbar").addClass("navbar-fixed-bottom");

        // Toggle navbar style (default)
        $(".navbar").addClass("navbar-default");
        $(".navbar").removeClass("navbar-inverse");

        // Animations
        $(".navbar-brand").removeClass("fadeInUp");

    }
}

$(function(){

    /* Headroom
    -------------------------------------------------------- */
    $(".navbar").headroom({
        // vertical offset in px before element is first unpinned
        offset : $(window).height() + 100,
        // scroll tolerance in px before state changes
        tolerance : 0,
        // css classes to apply
        classes : {
            // when element is initialised
            initial : "animated",
            // when scrolling up
            pinned : "fadeInDown",
            // when scrolling down
            unpinned : "fadeOutUp"
        }
    });

    /* Waypoints
    -------------------------------------------------------- */
    $.fn.waypoint.defaults = {
      context: window,
      continuous: true,
      enabled: true,
      horizontal: false,
      offset: 330,
      triggerOnce: true
    }
    /* Page section animation */
    $(".page-section").css("opacity", 0);
    $('.page-section').waypoint(function() {
        animate(this);
    });
    function animate(element){
        $(element).addClass("animated fadeIn");
    }

    /* Toggle Cover Overlay
    -------------------------------------------------------- */
    $(".toggle-cover-overlay i").tooltip();
    $(".toggle-cover-overlay").click(function(){
        $(".carousel-valtari i").toggleClass("fa-eye");
        $(".carousel-valtari i").toggleClass("fa-eye-slash");
        $(".carousel-valtari").toggleClass("carousel-overlay");
    });

    /* Google Maps
    -------------------------------------------------------- */
    google.maps.event.addDomListener(window, 'load', init);

    function init() {
        // Basic options for a simple Google Map
        // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
        var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: 11,

            scrollwheel: false,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: false,
            draggable: true,

            // The latitude and longitude to center the map (always required)
            center: new google.maps.LatLng(40.6700, -73.9400), // New York

            // How you would like to style the map. 
            // This is where you would paste any style found on Snazzy Maps.
            styles: [{featureType:"landscape",stylers:[{saturation:-100},{lightness:65},{visibility:"on"}]},{featureType:"poi",stylers:[{saturation:-100},{lightness:51},{visibility:"simplified"}]},{featureType:"road.highway",stylers:[{saturation:-100},{visibility:"simplified"}]},{featureType:"road.arterial",stylers:[{saturation:-100},{lightness:30},{visibility:"on"}]},{featureType:"road.local",stylers:[{saturation:-100},{lightness:40},{visibility:"on"}]},{featureType:"transit",stylers:[{saturation:-100},{visibility:"simplified"}]},{featureType:"administrative.province",stylers:[{visibility:"off"}]/**/},{featureType:"administrative.locality",stylers:[{visibility:"off"}]},{featureType:"administrative.neighborhood",stylers:[{visibility:"on"}]/**/},{featureType:"water",elementType:"labels",stylers:[{visibility:"on"},{lightness:-25},{saturation:-100}]},{featureType:"water",elementType:"geometry",stylers:[{hue:"#ffff00"},{lightness:-25},{saturation:-97}]}]
        };

        // Get the HTML DOM element that will contain your map 
        // We are using a div with id="map" seen below in the <body>
        var mapElement = document.getElementById('map');

        // Create the Google Map using out element and options defined above
        var map = new google.maps.Map(mapElement, mapOptions);
    }

    /* Portfolio (Isotope)
    -------------------------------------------------------- */
    var container = $('#portfolio-items');
    
    // Portfolio: Number of columns
    function portfolioColumnNumber() {
            if (windowWidth < 768) {
            var portfolioColumns = 2;
        }
        else if (windowWidth >= 768) {
            var portfolioColumns = 3;
        }
        return portfolioColumns;
    }
    
    // Initialize Isotope & Masonry Layout
    container.imagesLoaded( function() {
        container.isotope({
          itemSelector: 'li',
          resizable: false,  // disable normal resizing
          masonry: { columnWidth: container.width() / portfolioColumnNumber }
         });
     });
    
    // Update portfolio layout when resizing the browser window
    $(window).smartresize(function() {

      container.isotope({
        masonry: { columnWidth: container.width() / portfolioColumnNumber }
      });
    });
    
    $(window).smartresize();

    // Initialize Portfolio Filter (on click)
    $('#portfolio-filter a').click(function(e) {        
        e.preventDefault();   
      var selector = $(this).attr('data-filter');
      container.isotope({ filter: selector });      
        // Active Filter Class
        $('#portfolio-filter').find('.active').removeClass('active');
        $(this).parent().addClass('active');        
        return false;       
    });
});